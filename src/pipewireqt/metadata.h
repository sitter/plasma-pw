// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

#pragma once

#include "pipewireobject.h"
#include "pipewireqt_export.h"

namespace PipeWireQt
{

class MetadataPrivate;

class PIPEWIREQT_EXPORT Metadata : public PipeWireObject
{
    Q_OBJECT
    Q_PROPERTY(QMap<QString, QString> metadata READ metadata NOTIFY metadataChanged)
public:
    explicit Metadata(const std::shared_ptr<MetadataPrivate> &dptr, QObject *parent = nullptr);
    ~Metadata() override;
    Q_DISABLE_COPY_MOVE(Metadata)

    [[nodiscard]] QMap<QString, QString> metadata() const;

Q_SIGNALS:
    void metadataChanged();

private:
    std::shared_ptr<MetadataPrivate> d;
};

} // namespace PipeWireQt
