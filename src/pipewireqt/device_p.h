// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

#pragma once

#include "device.h"

#include <pipewire/device.h>
#include <spa/pod/iter.h>
#include <spa/debug/pod.h>
#include <spa/pod/parser.h>

#include "pipewireobject_p.h"

namespace PipeWireQt
{

class Profile : public QObject
{
    Q_OBJECT
    Q_PROPERTY(uint32_t index MEMBER m_index CONSTANT)
    Q_PROPERTY(QString name MEMBER m_name CONSTANT)
    Q_PROPERTY(QString description MEMBER m_description CONSTANT)
    Q_PROPERTY(uint32_t priority MEMBER m_priority CONSTANT)
    Q_PROPERTY(uint32_t available MEMBER m_available CONSTANT)
public:
    // using Availability = spa_param_availability;
    // Q_ENUM(Availability)

    using QObject::QObject;

    uint32_t m_index;
    QString m_name;
    QString m_description;
    uint32_t m_priority;
    uint32_t m_available;
};

class Route : public QObject {
    Q_OBJECT
    Q_PROPERTY(uint32_t index MEMBER m_index CONSTANT)
    Q_PROPERTY(QString name MEMBER m_name CONSTANT)
    Q_PROPERTY(QString description MEMBER m_description CONSTANT)
    Q_PROPERTY(uint32_t priority MEMBER m_priority CONSTANT)
    Q_PROPERTY(uint32_t available MEMBER m_available CONSTANT)
public:
    using Availability = spa_param_availability;
    Q_ENUM(Availability)

    using QObject::QObject;

    uint32_t m_index;
    QString m_name;
    QString m_description;
    uint32_t m_priority;
    uint32_t m_available;
};

class DevicePrivate : public PipeWireObjectPrivate
{
public:
    using PipeWireObjectPrivate::PipeWireObjectPrivate;

    void init() override;

    void info(const struct pw_device_info *info)
    {
        if (info->change_mask & PW_DEVICE_CHANGE_MASK_PROPS) {
            m_properties = makeProperties(info->props);
            Q_EMIT q->propertiesChanged();
        }

        if (info->change_mask & PW_DEVICE_CHANGE_MASK_PARAMS) {
            for (const auto parameter : std::span{info->params, info->n_params}) {
                auto x = pw_device_enum_params(m_device, 0, parameter.id, 0, -1, nullptr);
            }
        }
    }

    void param(int seq, uint32_t id, uint32_t index, uint32_t next, const struct spa_pod *param)
    {
        // qDebug() << seq << id << index << next << SPA_POD_TYPE(param);
        // spa_debug_pod(0, nullptr, param);

        switch (id) {
        case SPA_PARAM_EnumProfile: {
            auto profile = std::make_shared<Profile>();
            struct spa_pod_parser parser{};
            spa_pod_parser_pod(&parser, param);
            char *name = nullptr;
            char *description = nullptr;
		    auto ret = spa_pod_parse_object(param,
				SPA_TYPE_OBJECT_ParamProfile, nullptr,
				SPA_PARAM_PROFILE_index, SPA_POD_Int(&profile->m_index),
				SPA_PARAM_PROFILE_name, SPA_POD_String(&name),
				SPA_PARAM_PROFILE_description, SPA_POD_OPT_String(&description),
				SPA_PARAM_PROFILE_priority, SPA_POD_OPT_Int(&profile->m_priority),
				SPA_PARAM_PROFILE_available, SPA_POD_OPT_Id(&profile->m_available));
            profile->m_name = QString::fromUtf8(name);
            profile->m_description = QString::fromUtf8(description);
            m_profiles.insert(index, profile);

            Q_EMIT q->profilesChanged();
            break;
        }

        case SPA_PARAM_Profile: {
            struct spa_pod_parser parser{};
            spa_pod_parser_pod(&parser, param);
            auto ret = spa_pod_parse_object(param, SPA_TYPE_OBJECT_ParamProfile, nullptr, SPA_PARAM_PROFILE_index, SPA_POD_Int(&m_activeProfileIndex));

            Q_EMIT q->activeProfileIndexChanged();
            break;
        }

        case SPA_PARAM_EnumRoute: {
            auto route = std::make_shared<Route>();
            struct spa_pod_parser parser{};
            spa_pod_parser_pod(&parser, param);
            char *name = nullptr;
            char *description = nullptr;
		    auto ret = spa_pod_parse_object(param,
				SPA_TYPE_OBJECT_ParamRoute, nullptr,
				SPA_PARAM_ROUTE_index, SPA_POD_Int(&route->m_index),
				// SPA_PARAM_ROUTE_device, SPA_POD_String(&name),
                SPA_PARAM_ROUTE_name, SPA_POD_OPT_String(&name),
				SPA_PARAM_ROUTE_description, SPA_POD_OPT_String(&description),
				SPA_PARAM_ROUTE_priority, SPA_POD_OPT_Int(&route->m_priority),
				SPA_PARAM_PROFILE_available, SPA_POD_OPT_Id(&route->m_available));
            route->m_name = QString::fromUtf8(name);
            route->m_description = QString::fromUtf8(description);
            m_routes.insert(index, route);

            Q_EMIT q->routesChanged();
            break;
        }

        case SPA_PARAM_Route: {
            auto route = std::make_shared<Route>();
            struct spa_pod_parser parser{};
            spa_pod_parser_pod(&parser, param);
            auto ret = spa_pod_parse_object(param, SPA_TYPE_OBJECT_ParamRoute, nullptr, SPA_PARAM_ROUTE_index, SPA_POD_Int(&m_activeRouteIndex));

            Q_EMIT q->activeRouteIndexChanged();
            break;
        }

        }

    }

    Device *q = nullptr;
    spa_hook m_listener;
    pw_device *m_device = nullptr;
    QMap<uint32_t, std::shared_ptr<Profile>> m_profiles;
    uint32_t m_activeProfileIndex = 0;
    QMap<uint32_t, std::shared_ptr<Route>> m_routes;
    uint32_t m_activeRouteIndex = 0;
};

} // namespace PipeWireQt
