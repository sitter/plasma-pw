// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

#pragma once

#include "pipewireobject.h"
#include "pipewireqt_export.h"

namespace PipeWireQt
{

class DevicePrivate;

class PIPEWIREQT_EXPORT Device : public PipeWireObject
{
    Q_OBJECT
    Q_PROPERTY(QList<QObject *> profiles READ profiles NOTIFY profilesChanged)
    Q_PROPERTY(uint32_t activeProfileIndex READ activeProfileIndex WRITE setActiveProfileIndex NOTIFY activeProfileIndexChanged)
    Q_PROPERTY(QList<QObject *> routes READ routes NOTIFY routesChanged)
    Q_PROPERTY(uint32_t activeRouteIndex READ activeRouteIndex WRITE setActiveRouteIndex NOTIFY activeRouteIndexChanged)
public:
    explicit Device(const std::shared_ptr<DevicePrivate> &dptr, QObject *parent = nullptr);
    ~Device() override;
    Q_DISABLE_COPY_MOVE(Device)

    [[nodiscard]] QList<QObject *> profiles() const;
    [[nodiscard]] uint32_t activeProfileIndex() const;
    void setActiveProfileIndex(uint32_t index);
    [[nodiscard]] QList<QObject *> routes() const;
    [[nodiscard]] uint32_t activeRouteIndex() const;
    void setActiveRouteIndex(uint32_t index);

Q_SIGNALS:
    void profilesChanged();
    void activeProfileIndexChanged();
    void routesChanged();
    void activeRouteIndexChanged();

private:
    std::shared_ptr<DevicePrivate> d;
};

} // namespace PipeWireQt
