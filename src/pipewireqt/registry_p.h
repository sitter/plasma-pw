// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

#pragma once

#include <pipewire/core.h>

#warning fixme temporary for node debugging
#include <pipewire/pipewire.h>
#include <spa/param/props.h>
#include <spa/param/audio/raw.h>
#include <spa/pod/builder.h>

#include <memory>
#include <span>

#include <QJSEngine>

#include <QDebug>
#include "client.h"
#include "client_p.h"
#include "pipewireobject.h"
#include "node.h"
#include "node_p.h"
#include "device.h"
#include "device_p.h"
#include "port.h"
#include "port_p.h"
#include "metadata.h"
#include "metadata_p.h"

namespace std
{
template<>
struct default_delete<pw_registry> {
    void operator()(pw_registry *ptr) const
    {
        pw_proxy_destroy(reinterpret_cast<pw_proxy *>(ptr));
    }
};
} // namespace std

namespace PipeWireQt
{

class RegistryPrivate : public QObject
{
    Q_OBJECT
public:
    explicit RegistryPrivate(pw_core *core);

    ~RegistryPrivate()
    {
        qDebug() << Q_FUNC_INFO;
        spa_hook_remove(&m_registryListener);
    }

    template<typename T, typename TPrivate, typename... Args>
    void makeObject(Args...args)
    {
        auto dptr = std::make_shared<TPrivate>(std::forward<Args>(args)..., this);
        dptr->init();
        auto object = std::make_shared<T>(dptr);
        QJSEngine::setObjectOwnership(object.get(), QJSEngine::CppOwnership);
        m_objects.emplace_back(object);
    }

    template<typename... Args>
    void global(Args... args)
    {
        auto [id, permissions, type, version, props] = std::tuple(args...);

        // qDebug() << Q_FUNC_INFO;
        // qDebug() << id << permissions << type << version;
        // for (const auto &prop : std::span{props->items, props->n_items}) {
        //     qDebug() << prop.key << prop.value;
        // }

        static constexpr auto router = {
            std::make_pair(PW_TYPE_INTERFACE_Client, &RegistryPrivate::makeObject<Client, ClientPrivate, Args...>),
            std::make_pair(PW_TYPE_INTERFACE_Node, &RegistryPrivate::makeObject<Node, NodePrivate, Args...>),
            std::make_pair(PW_TYPE_INTERFACE_Device, &RegistryPrivate::makeObject<Device, DevicePrivate, Args...>),
            std::make_pair(PW_TYPE_INTERFACE_Port, &RegistryPrivate::makeObject<Port, PortPrivate, Args...>),
            std::make_pair(PW_TYPE_INTERFACE_Metadata, &RegistryPrivate::makeObject<Metadata, MetadataPrivate, Args...>),
        };
        for (const auto &[interface, maker] : router) {
            if (spa_streq(type, interface)) {
                (this->*(maker))(std::forward<Args>(args)...);
            }
        }

#warning garbage signal spam for first objects notifications
        Q_EMIT objectsChanged();
    }

    void global_remove(uint32_t id)
    {
        std::erase_if(m_objects, [id](const auto &object) {
            return id == object->id();
        });
        Q_EMIT objectsChanged();
    }

    std::unique_ptr<pw_registry> m_registry;
    spa_hook m_registryListener{};
    std::vector<std::shared_ptr<PipeWireObject>> m_objects;

Q_SIGNALS:
    void objectsChanged();
};

namespace RegistryEvents
{
template<typename... Args>
void global(auto data, Args... args)
{
    static_cast<RegistryPrivate *>(data)->global(std::forward<Args>(args)...);
}

template<typename... Args>
void global_remove(auto data, Args... args)
{
    static_cast<RegistryPrivate *>(data)->global_remove(std::forward<Args>(args)...);
}
} // namespace RegistryEvents

} // namespace PipeWireQt
