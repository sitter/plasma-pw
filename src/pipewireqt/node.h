// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

#pragma once

#include <QVariantMap>

#include "pipewireobject.h"
#include "pipewireqt_export.h"

namespace PipeWireQt
{

class NodePrivate;
class Device;

class PIPEWIREQT_EXPORT Node : public PipeWireObject
{
    Q_OBJECT
    Q_PROPERTY(uint32_t maxInputPorts READ maxInputPorts NOTIFY changed)
    Q_PROPERTY(uint32_t maxOutputPorts READ maxOutputPorts NOTIFY changed)
    Q_PROPERTY(uint32_t inputPorts READ inputPorts NOTIFY inputPortsChanged)
    Q_PROPERTY(uint32_t outputPorts READ outputPorts NOTIFY outputPortsChanged)
    // Q_PROPERTY(pw_node_state state READ state NOTIFY stateChanged)
    Q_PROPERTY(QString error READ error NOTIFY errorChanged)
    // Q_PROPERTY(QVariantMap parameters READ parameters NOTIFY parametersChanged)

    // Extra properties for convenience
    Q_PROPERTY(Device *device READ device NOTIFY propertiesChanged);

    // Parameter Properties
    Q_PROPERTY(float volume READ volume WRITE setVolume NOTIFY volumeChanged)
    Q_PROPERTY(bool muted READ isMuted WRITE setMuted NOTIFY mutedChanged)
    // Q_PROPERTY(bool volumeWritable READ isVolumeWritable NOTIFY isVolumeWritableChanged)
    Q_PROPERTY(QStringList channels READ channels NOTIFY channelsChanged)
    Q_PROPERTY(QVector<float> channelVolumes READ channelVolumes WRITE setChannelVolumes NOTIFY channelVolumesChanged)
    Q_PROPERTY(QStringList rawChannels READ rawChannels NOTIFY rawChannelsChanged)

    // TODO move somewhere else this doesn't really belong on node
    Q_PROPERTY(QString defaultSinkName READ defaultSinkName WRITE setDefaultSinkName NOTIFY defaultSinkNameChanged)

public:
    explicit Node(const std::shared_ptr<NodePrivate> &dptr, QObject *parent = nullptr);
    ~Node() override;
    Q_DISABLE_COPY_MOVE(Node)

    [[nodiscard]] uint32_t maxInputPorts() const;
    [[nodiscard]] uint32_t maxOutputPorts() const;
    [[nodiscard]] uint32_t inputPorts() const;
    [[nodiscard]] uint32_t outputPorts() const;
    // [[nodiscard]] pw_node_state state() const;
    [[nodiscard]] QString error() const;
    // [[nodiscard]] QVariantMap parameters() const;
    [[nodiscard]] Device *device() const;

    [[nodiscard]] float volume() const;
    void setVolume(float volume);
    [[nodiscard]] bool isMuted() const;
    void setMuted(bool mute);
    [[nodiscard]] QStringList channels() const;
    [[nodiscard]] QList<float> channelVolumes() const;
    void setChannelVolumes(QList<float> volumes);
    [[nodiscard]] QStringList rawChannels() const;
    [[nodiscard]] QString defaultSinkName() const;
    void setDefaultSinkName(const QString &name);

public Q_SLOTS:
    // Extra helper
    virtual int setChannelVolume(int channel, float volume);

Q_SIGNALS:
    void changed();
    void inputPortsChanged();
    void outputPortsChanged();
    void stateChanged();
    void errorChanged();
    void parametersChanged();

    void volumeChanged();
    void mutedChanged();
    void isVolumeWritableChanged();
    void channelsChanged();
    void channelVolumesChanged();
    void rawChannelsChanged();
    void defaultSinkNameChanged();

private:
    std::shared_ptr<NodePrivate> d;
    friend class SinkAdaptor;
};

} // namespace PipeWireQt
