// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

#include "context.h"

#include <pipewire/pipewire.h>

#include <QSocketNotifier>

#include "core.h"
#include "core_p.h"
#include "initializer.h"

namespace PipeWireQt
{

class Loop : public QObject
{
    Q_OBJECT
public:
    explicit Loop()
        : m_pw_loop(pw_loop_new(nullptr), pw_loop_destroy)
        , m_readNotifier(pw_loop_get_fd(m_pw_loop), QSocketNotifier::Read)
        , m_writeNotifier(pw_loop_get_fd(m_pw_loop), QSocketNotifier::Write)
    {
        connect(&m_readNotifier, &QSocketNotifier::activated, this, &Loop::iterate);
        connect(&m_writeNotifier, &QSocketNotifier::activated, this, &Loop::iterate);
    }

    void iterate() const
    {
        pw_loop_enter(m_pw_loop);
        if (pw_loop_iterate(m_pw_loop, 0) < 0) {
            qWarning() << Q_FUNC_INFO << "iterate failed";
        }
        pw_loop_leave(m_pw_loop);
    }

    std::unique_ptr<pw_loop, decltype(&pw_loop_destroy)> m_pw_loop;
    QSocketNotifier m_readNotifier;
    QSocketNotifier m_writeNotifier;
};

class ContextPrivate
{
public:
    ContextPrivate()
        : m_context(makeContext(), pw_context_destroy)
        , m_core(std::make_unique<CorePrivate>(pw_context_connect(m_context.get(), nullptr, 0)))
    {
    }

    pw_context *makeContext()
    {
        auto dict_items = std::to_array<spa_dict_item>({
            {PW_KEY_MEDIA_TYPE, "Audio"},
            {PW_KEY_MEDIA_CATEGORY, "Manager"},
        });
        auto dict = SPA_DICT_INIT(dict_items.data(), dict_items.size());
        auto properties = pw_properties_new_dict(&dict);
        #warning fixme this surely leaks

        return pw_context_new(m_loop.m_pw_loop.get(), properties, 0);
    }

    bool m_initialized = Initializer::instance(); // do this first so it always happens as first call
    Loop m_loop;
    std::unique_ptr<pw_context, decltype(&pw_context_destroy)> m_context;
    Core m_core;
};

} // namespace PipeWireQt
