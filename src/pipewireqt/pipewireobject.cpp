// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2014-2024 Harald Sitter <sitter@kde.org>

#include "pipewireobject.h"
#include "pipewireobject_p.h"

namespace PipeWireQt
{

PipeWireObjectPrivate::PipeWireObjectPrivate(uint32_t id,
                                             uint32_t permissions,
                                             const char *type,
                                             uint32_t version,
                                             const struct spa_dict *props,
                                             RegistryPrivate *registry)
    : m_registry(registry)
    , m_id(id)
    , m_permissions(permissions)
    , m_type(type)
    , m_version(version)
    , m_properties(makeProperties(props))
{
}

PipeWireObject::PipeWireObject(const std::shared_ptr<PipeWireObjectPrivate> &dptr, QObject *parent)
    : QObject(parent)
    , d(dptr)
{
}

PipeWireObject::~PipeWireObject() = default;

QString PipeWireObject::type() const
{
    return d->m_type;
}

QVariantMap PipeWireObject::properties() const
{
    return d->m_properties;
}

uint32_t PipeWireObject::id() const
{
    return d->m_id;
}

} // namespace PipeWireQt
