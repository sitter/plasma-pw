// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

#pragma once

#include <pipewire/core.h>

#include <memory>

#include "registry_p.h"

namespace PipeWireQt
{

class CorePrivate
{
public:
    explicit CorePrivate(pw_core *core)
        : m_core(core, pw_core_disconnect)
        , m_registry(m_core.get())
    {
    }

    std::unique_ptr<pw_core, decltype(&pw_core_disconnect)> m_core;
    RegistryPrivate m_registry;
};

} // namespace PipeWireQt
