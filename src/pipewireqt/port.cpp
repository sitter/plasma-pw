// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

#include "port.h"
#include "port_p.h"

#include <pipewire/node.h>

#include "pipewireobject_p.h"
#include "registry_p.h"

namespace PipeWireQt
{

namespace PortEvents
{
template<typename... Args>
void info(auto data, Args... args)
{
    static_cast<PortPrivate *>(data)->info(std::forward<Args>(args)...);
}

template<typename... Args>
void param(auto data, Args... args)
{
    static_cast<PortPrivate *>(data)->param(std::forward<Args>(args)...);
}
} // namespace PortEvents

void PortPrivate::init()
{
    static const struct pw_port_events portEvents = {
        .info = PortEvents::info,
        .param = PortEvents::param,
    };
    m_port = static_cast<pw_port *>(pw_registry_bind(m_registry->m_registry.get(), m_id, qUtf8Printable(m_type), PW_VERSION_PORT, 0));
    spa_zero(m_listener);
    pw_port_add_listener(m_port, &m_listener, &portEvents, this);
}

Port::Port(const std::shared_ptr<PortPrivate> &dptr, QObject *parent)
    : PipeWireObject(dptr, parent)
    , d(dptr)
{
    d->q = this;
}

Port::~Port() = default;

} // namespace PipeWireQt
