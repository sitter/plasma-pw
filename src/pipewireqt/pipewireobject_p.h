// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2014-2024 Harald Sitter <sitter@kde.org>
// SPDX-FileCopyrightText: 2018 Nicolas Fella <nicolas.fella@gmx.de>

#pragma once

#include <pipewire/client.h>

#include <span>

#include <QVariantMap>

namespace PipeWireQt
{

class RegistryPrivate;

class PipeWireObjectPrivate
{
public:
    PipeWireObjectPrivate(uint32_t id, uint32_t permissions, const char *type, uint32_t version, const struct spa_dict *props, RegistryPrivate *registry);
    virtual ~PipeWireObjectPrivate() = default;
    Q_DISABLE_COPY_MOVE(PipeWireObjectPrivate)

    virtual void init()
    {
    }

    QVariantMap makeProperties(const struct spa_dict *props)
    {
        QVariantMap properties;
        for (const auto &prop : std::span{props->items, props->n_items}) {
            properties.insert(QString::fromUtf8(prop.key), QString::fromUtf8(prop.value));
        }
        return properties;
    }

    RegistryPrivate *m_registry;
    uint32_t m_id;
    uint32_t m_permissions;
    QString m_type;
    uint32_t m_version;
    QVariantMap m_properties;
};

} // namespace PipeWireQt
