// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

#pragma once

#include "port.h"

#include <pipewire/port.h>
#include <spa/pod/iter.h>
#include <spa/debug/pod.h>
#include <spa/pod/parser.h>

#include "pipewireobject_p.h"

namespace PipeWireQt
{

class PortPrivate : public PipeWireObjectPrivate
{
public:
    using PipeWireObjectPrivate::PipeWireObjectPrivate;

    void init() override;

    void info(const struct pw_port_info *info)
    {
        if (info->change_mask & PW_PORT_CHANGE_MASK_PROPS) {
            m_properties = makeProperties(info->props);
            Q_EMIT q->propertiesChanged();
        }

        if (info->change_mask & PW_PORT_CHANGE_MASK_PARAMS) {
            for (const auto parameter : std::span{info->params, info->n_params}) {
                auto x = pw_port_enum_params(m_port, 0, parameter.id, 0, -1, nullptr);
            }
        }
    }

    void param(int seq, uint32_t id, uint32_t index, uint32_t next, const struct spa_pod *param)
    {
        // spa_debug_pod(0, nullptr, param);
    }

    Port *q = nullptr;
    spa_hook m_listener;
    pw_port *m_port = nullptr;
};

} // namespace PipeWireQt
