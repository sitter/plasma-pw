// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

#pragma once

#include "pipewireobject.h"
#include "pipewireqt_export.h"

namespace PipeWireQt
{

class ClientPrivate;

class PIPEWIREQT_EXPORT Client : public PipeWireObject
{
    Q_OBJECT
public:
    explicit Client(const std::shared_ptr<ClientPrivate> &dptr, QObject *parent = nullptr);
    ~Client() override;
    Q_DISABLE_COPY_MOVE(Client)

private:
    std::shared_ptr<ClientPrivate> d;
};

} // namespace PipeWireQt
