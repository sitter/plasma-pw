// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

#pragma once

#include "node.h"

#include <pipewire/node.h>
#include <spa/debug/pod.h>

#include "pipewireobject_p.h"
#include "registry_p.h"

namespace PipeWireQt
{

class NodePrivate : public PipeWireObjectPrivate
{
public:
    using PipeWireObjectPrivate::PipeWireObjectPrivate;

    void init() override;

    void info(const struct pw_node_info *info)
    {
        if (info->max_input_ports != m_maxInputPorts) {
            m_maxInputPorts = info->max_input_ports;
            Q_EMIT q->changed();
        }
        if (info->max_output_ports != m_maxOutputPorts) {
            m_maxOutputPorts = info->max_output_ports;
            Q_EMIT q->changed();
        }

        if (info->change_mask & PW_NODE_CHANGE_MASK_INPUT_PORTS) {
            m_inputPorts = info->n_input_ports;
            Q_EMIT q->inputPortsChanged();
        }

        if (info->change_mask & PW_NODE_CHANGE_MASK_OUTPUT_PORTS) {
            m_outputPorts = info->n_output_ports;
            Q_EMIT q->outputPortsChanged();
        }

        if (info->change_mask & PW_NODE_CHANGE_MASK_STATE) {
            m_state = info->state;
            Q_EMIT q->stateChanged();
        }

        if (info->change_mask & PW_NODE_CHANGE_MASK_PROPS) {
            m_properties = makeProperties(info->props);
            Q_EMIT q->propertiesChanged();
        }

        if (info->change_mask & PW_DEVICE_CHANGE_MASK_PARAMS) {
            for (const auto parameter : std::span{info->params, info->n_params}) {
                auto x = pw_device_enum_params(m_node, 0, parameter.id, 0, -1, nullptr);
            }
        }
    }

    void param(int seq, uint32_t id, uint32_t index, uint32_t next, const struct spa_pod *param)
    {
        qDebug() << "&&&&&&&&&&&&&&& Node Params";
        qDebug() << seq << id << index << next;
        // spa_debug_pod(0, nullptr, param);


		switch (id) {
		// case SPA_PARAM_EnumFormat:
		// case SPA_PARAM_Format:

        case SPA_PARAM_Props: {
            qDebug() << "!!!!!!!!!!!!!!!!! props";
            struct spa_pod_object *obj = (struct spa_pod_object *) param;
            struct spa_pod_prop *prop;

            SPA_POD_OBJECT_FOREACH(obj, prop)
            {
                qDebug() << prop->key;
                switch (prop->key) {
                case SPA_PROP_volume: {
                    auto ret = spa_pod_get_float(&prop->value, &m_volume);
#warning fixme do we care about SPA_POD_PROP_FLAG_HARDWARE

                    Q_EMIT q->volumeChanged();
                    break;
                }
                case SPA_PROP_mute: {
                    auto ret = spa_pod_get_bool(&prop->value, &m_muted);
#warning fixme do we care about SPA_POD_PROP_FLAG_HARDWARE

                    Q_EMIT q->mutedChanged();
                    break;
                }
                case SPA_PROP_channelVolumes: {
                    qDebug() << "!!!!!!!!!!!!!!!!! channel volumes";
                    m_channelCount = spa_pod_copy_array(&prop->value, SPA_TYPE_Float, m_channelVolumes.data(), m_channelVolumes.size());
#warning fixme do we care about SPA_POD_PROP_FLAG_HARDWARE

                    Q_EMIT q->channelVolumesChanged();
                    Q_EMIT q->channelsChanged();
                    Q_EMIT q->rawChannelsChanged();
                    break;
                }
                case SPA_PROP_channelMap:
                    m_channelCount = spa_pod_copy_array(&prop->value, SPA_TYPE_Id, m_channelMap.data(), m_channelMap.size());

                    Q_EMIT q->channelVolumesChanged();
                    Q_EMIT q->channelsChanged();
                    Q_EMIT q->rawChannelsChanged();
                    break;
                default:
                    break;
                }
            }
        }
        }
    }

    Node *q = nullptr;
    spa_hook m_listener;
    pw_node *m_node;

    uint32_t m_maxInputPorts;
    uint32_t m_maxOutputPorts;
    uint32_t m_inputPorts;
    uint32_t m_outputPorts;
    pw_node_state m_state;
    QString m_error;

    // Parameter Properties
    float m_volume = -1;
    bool m_muted = false;
    uint8_t m_channelCount = 0;
    std::array<float, SPA_AUDIO_MAX_CHANNELS> m_channelVolumes;
    std::array<uint32_t, SPA_AUDIO_MAX_CHANNELS> m_channelMap;

};

} // namespace PipeWireQt
