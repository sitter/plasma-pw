// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

#pragma once

#include "pipewireobject_p.h"

namespace PipeWireQt
{

class RegistryPrivate;

class ClientPrivate : public PipeWireObjectPrivate
{
public:
    using PipeWireObjectPrivate::PipeWireObjectPrivate;
};

} // namespace PipeWireQt
