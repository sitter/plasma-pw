// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2014-2024 Harald Sitter <sitter@kde.org>

#pragma once

#include <QObject>

#include "pipewireqt_export.h"

namespace PipeWireQt
{

class PipeWireObjectPrivate;

/**
 * Base class for most PulseAudio objects.
 */
class PIPEWIREQT_EXPORT PipeWireObject : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariantMap properties READ properties NOTIFY propertiesChanged)
public:
    explicit PipeWireObject(const std::shared_ptr<PipeWireObjectPrivate> &dptr, QObject *parent = nullptr);
    ~PipeWireObject() override;
    Q_DISABLE_COPY_MOVE(PipeWireObject)

    /**
     * The unique id in the PipeWire registry
     */
    [[nodiscard]] uint32_t id() const;

    /**
     * The type string identifier. e.g. ((TBD)).
     * This string is never empty.
     */
    [[nodiscard]] QString type() const;

    /**
     * A map of properties associated with this object.
     * The set of available properties depends on the type of object.
     */
    [[nodiscard]] QVariantMap properties() const;

Q_SIGNALS:
    void propertiesChanged();

private:
    std::shared_ptr<PipeWireObjectPrivate> d;
};

} // namespace PipeWireQt
