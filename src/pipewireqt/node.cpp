// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

#include "node.h"
#include "node_p.h"

#include <pipewire/node.h>

#include <span>

#include "pipewireobject_p.h"
#include "registry_p.h"

#warning pipewire has no channel map pretty names
#include <pulse/channelmap.h>

#include "metadata.h"

using namespace Qt::StringLiterals;

namespace PipeWireQt
{

namespace NodeEvents
{
template<typename... Args>
void info(auto data, Args... args)
{
    static_cast<NodePrivate *>(data)->info(std::forward<Args>(args)...);
}

template<typename... Args>
void param(auto data, Args... args)
{
    static_cast<NodePrivate *>(data)->param(std::forward<Args>(args)...);
}
} // namespace NodeEvents

void NodePrivate::init()
{
    static const struct pw_node_events nodeEvents = {
        .version = PW_VERSION_NODE_EVENTS,
        .info = NodeEvents::info,
        .param = NodeEvents::param,
    };
    m_node = static_cast<pw_node *>(pw_registry_bind(m_registry->m_registry.get(), m_id, qUtf8Printable(m_type), PW_VERSION_NODE, 0));
    spa_zero(m_listener);
    pw_node_add_listener(m_node, &m_listener, &nodeEvents, this);
}

Node::Node(const std::shared_ptr<NodePrivate> &dptr, QObject *parent)
    : PipeWireObject(dptr, parent)
    , d(dptr)
{
    d->q = this;
}

Node::~Node() = default;

uint32_t Node::maxInputPorts() const
{
    return d->m_maxInputPorts;
}

uint32_t Node::maxOutputPorts() const
{
    return d->m_maxOutputPorts;
}

uint32_t Node::inputPorts() const
{
    return d->m_inputPorts;
}

uint32_t Node::outputPorts() const
{
    return d->m_outputPorts;
}

QString Node::error() const
{
    return d->m_error;
}

Device *Node::device() const
{
    auto deviceId = d->m_properties.value("device.id");
    auto it = std::ranges::find_if(d->m_registry->m_objects, [deviceId](const auto &object) {
        return deviceId == object->id();
    });
    if (it == d->m_registry->m_objects.cend()){
        return nullptr;
    }
    return qobject_cast<Device *>(it->get());
}

float Node::volume() const
{
    return d->m_volume;
}

void Node::setVolume(float volume)
{
}

bool Node::isMuted() const
{
    return d->m_muted;
}

void Node::setMuted(bool mute)
{
}

QStringList Node::channels() const
{
    QStringList list;

    static QMap map{
        std::make_pair(SPA_AUDIO_CHANNEL_UNKNOWN, PA_CHANNEL_POSITION_INVALID),
        std::make_pair(SPA_AUDIO_CHANNEL_MONO, PA_CHANNEL_POSITION_MONO),
        std::make_pair(SPA_AUDIO_CHANNEL_FL, PA_CHANNEL_POSITION_FRONT_LEFT),
        std::make_pair(SPA_AUDIO_CHANNEL_FR, PA_CHANNEL_POSITION_FRONT_RIGHT),
        std::make_pair(SPA_AUDIO_CHANNEL_FC, PA_CHANNEL_POSITION_FRONT_CENTER),
    };

    for (const auto &channelId : std::span{d->m_channelMap.data(), d->m_channelCount}) {
        auto pa_channel = map.value(static_cast<spa_audio_channel>(channelId), PA_CHANNEL_POSITION_INVALID);
        QString name = pa_channel_position_to_pretty_string(pa_channel);
        list.append(name);
    }

    return list;
}

QList<float> Node::channelVolumes() const
{
    QList<float> list;
    for (const auto &volume : std::span{d->m_channelVolumes.data(), d->m_channelCount}) {
        list.append(volume);
    }
    qDebug() << list;
    return list;
}

void Node::setChannelVolumes(QList<float> volumes)
{

}

QStringList Node::rawChannels() const
{
    return channels();
}

int Node::setChannelVolume(int channel, float volume)
{
    std::array<char, 1024> buffer;
    auto builder = SPA_POD_BUILDER_INIT(buffer.data(), buffer.size());

    if (!SPA_FLAG_IS_SET(d->m_permissions, PW_PERM_W | PW_PERM_X)) {
        qDebug() << "EACCES";
        return -EACCES;
    }

    struct spa_pod_frame f{};
    spa_pod_builder_push_object(&builder, &f, SPA_TYPE_OBJECT_Props, SPA_PARAM_Props);

    auto volumes = d->m_channelVolumes;
    volumes.at(channel) = volume;
    spa_pod_builder_add(&builder, SPA_PROP_channelVolumes, SPA_POD_Array(sizeof(float), SPA_TYPE_Float, d->m_channelCount, volumes.data()), 0);

	auto param = static_cast<spa_pod *>(spa_pod_builder_pop(&builder, &f));

    pw_device_set_param(d->m_node, SPA_PARAM_Props, 0, param);
	return 0;
}

QString Node::defaultSinkName() const
{
#warning this is all sorts of crap like what happens if the metadata disappears or changes...
    for (const auto &object : d->m_registry->m_objects) {
        if (object->type() != QLatin1String(PW_TYPE_INTERFACE_Metadata)) {
            continue;
        }
        if (object->properties().value(QLatin1String(PW_KEY_METADATA_NAME)) != u"default"_s) {
            continue;
        }
        auto metadata = qobject_cast<Metadata *>(object.get());
        if (!metadata) {
            qWarning() << "metadata cast failed";
            continue;
        }
#warning todo figure out what the difference between these two actually is
        if (const auto defaultSink = metadata->metadata().value("default.configured.audio.sink"); !defaultSink.isEmpty()) {
            return defaultSink;
        }
        return metadata->metadata().value("default.audio.sink");
    }
    return 0;
}

void Node::setDefaultSinkName(const QString &name)
{
}
} // namespace PipeWireQt
