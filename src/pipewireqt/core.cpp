// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

#include "core.h"
#include "core_p.h"

#include <pipewire/core.h>

namespace PipeWireQt
{

Core::Core(std::unique_ptr<CorePrivate> d_, QObject *parent)
    : QObject(parent)
    , d(std::move(d_))
{
}

Core::~Core() = default;

} // namespace PipeWireQt
