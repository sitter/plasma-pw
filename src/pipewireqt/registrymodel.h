// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2014-2024 Harald Sitter <sitter@kde.org>

#pragma once

#include <QAbstractListModel>

#include "pipewireqt_export.h"

#include "core.h"
#include "core_p.h"

namespace PipeWireQt
{

class RegistryModelPrivate;

class PIPEWIREQT_EXPORT RegistryModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QObject *context READ context WRITE setContext NOTIFY contextChanged)
public:
    enum ItemRole { PipeWireObjectRole = Qt::UserRole + 1, TypeRole, PropertiesRole };

    explicit RegistryModel(QObject *parent = nullptr);
    ~RegistryModel() override;

    [[nodiscard]] QHash<int, QByteArray> roleNames() const override;
    [[nodiscard]] int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    [[nodiscard]] QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    [[nodiscard]] QObject *context() const;
    void setContext(QObject *context);

Q_SIGNALS:
    void contextChanged();

private:
    std::unique_ptr<RegistryModelPrivate> d;
};

} // namespace PipeWireQt
