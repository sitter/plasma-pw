// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

#include "client.h"
#include "client_p.h"

#include <pipewire/client.h>

#include "registry_p.h"

namespace PipeWireQt
{

Client::Client(const std::shared_ptr<ClientPrivate> &dptr, QObject *parent)
    : PipeWireObject(dptr, parent)
    , d(dptr)
{
}

Client::~Client() = default;

} // namespace PipeWireQt
