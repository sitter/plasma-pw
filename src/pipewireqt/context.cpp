// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

#include "context.h"
#include "context_p.h"

namespace PipeWireQt
{

Context::Context(QObject *parent)
    : QObject(parent)
    , d(std::make_unique<ContextPrivate>())
{
}

Context::~Context() = default;

} // namespace PipeWireQt
