// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

#include "device.h"
#include "device_p.h"

#include <pipewire/node.h>

#include "pipewireobject_p.h"
#include "registry_p.h"

namespace PipeWireQt
{

namespace DeviceEvents
{
template<typename... Args>
void info(auto data, Args... args)
{
    static_cast<DevicePrivate *>(data)->info(std::forward<Args>(args)...);
}

template<typename... Args>
void param(auto data, Args... args)
{
    static_cast<DevicePrivate *>(data)->param(std::forward<Args>(args)...);
}
} // namespace DeviceEvents

void DevicePrivate::init()
{
    static const struct pw_device_events deviceEvents = {
        .version = PW_VERSION_DEVICE_EVENTS,
        .info = DeviceEvents::info,
        .param = DeviceEvents::param,
    };
    m_device = static_cast<pw_device *>(pw_registry_bind(m_registry->m_registry.get(), m_id, qUtf8Printable(m_type), PW_VERSION_DEVICE, 0));
    spa_zero(m_listener);
    pw_device_add_listener(m_device, &m_listener, &deviceEvents, this);
}

Device::Device(const std::shared_ptr<DevicePrivate> &dptr, QObject *parent)
    : PipeWireObject(dptr, parent)
    , d(dptr)
{
    d->q = this;
}

Device::~Device() = default;

QList<QObject *> Device::profiles() const
{
    QList<QObject *> list;
    for (const auto &[_, profile] : d->m_profiles.asKeyValueRange()) {
        list.append(profile.get());
    }
    return list;
}

uint32_t Device::activeProfileIndex() const
{
    return d->m_activeProfileIndex;
}

QList<QObject *> Device::routes() const
{
    QList<QObject *> list;
    for (const auto &[_, route] : d->m_routes.asKeyValueRange()) {
        list.append(route.get());
    }
    return list;
}

uint32_t Device::activeRouteIndex() const
{
    return d->m_activeRouteIndex;
}

void Device::setActiveProfileIndex(uint32_t index)
{
    if (!SPA_FLAG_IS_SET(d->m_permissions, PW_PERM_W | PW_PERM_X)) {
        qDebug() << "EACCES";
        return;
    }

    std::array<char, 1024> buffer{0};
    auto builder = SPA_POD_BUILDER_INIT(buffer.data(), buffer.size());
    auto param = static_cast<spa_pod *>(spa_pod_builder_add_object(&builder,
                                                                   SPA_TYPE_OBJECT_ParamProfile,
                                                                   SPA_PARAM_Profile,
                                                                   SPA_PARAM_PROFILE_index,
                                                                   SPA_POD_Int(index),
                                                                   SPA_PARAM_PROFILE_save,
                                                                   SPA_POD_Bool(true)));
    auto ret = pw_device_set_param(d->m_device, SPA_PARAM_Profile, 0, param);
}

void Device::setActiveRouteIndex(uint32_t index)
{
    if (!SPA_FLAG_IS_SET(d->m_permissions, PW_PERM_W | PW_PERM_X)) {
        qDebug() << "EACCES";
        return;
    }

    std::array<char, 1024> buffer{0};
    auto builder = SPA_POD_BUILDER_INIT(buffer.data(), buffer.size());
    auto param = static_cast<spa_pod *>(spa_pod_builder_add_object(&builder,
                                                                   SPA_TYPE_OBJECT_ParamRoute,
                                                                   SPA_PARAM_Route,
                                                                   SPA_PARAM_ROUTE_index,
                                                                   SPA_POD_Int(index),
                                                                   SPA_PARAM_ROUTE_device,
                                                                   SPA_POD_Int(id()),
                                                                   SPA_PARAM_ROUTE_save,
                                                                   SPA_POD_Bool(true)));
    auto ret = pw_device_set_param(d->m_device, SPA_PARAM_Route, 0, param);
}

} // namespace PipeWireQt
