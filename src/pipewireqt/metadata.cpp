// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

#include "metadata.h"
#include "metadata_p.h"

#include "registry_p.h"

namespace PipeWireQt
{

namespace MetadataEvents
{
template<typename... Args>
int property(auto data, Args... args)
{
    return static_cast<MetadataPrivate *>(data)->property(std::forward<Args>(args)...);
}
} // namespace MetadataEvents

void MetadataPrivate::init()
{
    static const struct pw_metadata_events metadataEvents = {
        .version = PW_VERSION_METADATA_EVENTS,
        .property = MetadataEvents::property,
    };
    m_metadata = static_cast<pw_metadata *>(pw_registry_bind(m_registry->m_registry.get(), m_id, qUtf8Printable(m_type), PW_VERSION_METADATA, 0));
    spa_zero(m_listener);
    pw_metadata_add_listener(m_metadata, &m_listener, &metadataEvents, this);
}

Metadata::Metadata(const std::shared_ptr<MetadataPrivate> &dptr, QObject *parent)
    : PipeWireObject(dptr, parent)
    , d(dptr)
{
    d->q = this;
}

Metadata::~Metadata() = default;

QMap<QString, QString> Metadata::metadata() const
{
    return d->m_data;
}

} // namespace PipeWireQt
