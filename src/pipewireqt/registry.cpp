// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

#include "registry_p.h"

namespace PipeWireQt
{

RegistryPrivate::RegistryPrivate(pw_core *core)
    : m_registry(pw_core_get_registry(core, PW_VERSION_REGISTRY, 0))
{
    static const struct pw_registry_events registryEvents = {
        .version = PW_VERSION_REGISTRY_EVENTS,
        .global = RegistryEvents::global,
        .global_remove = RegistryEvents::global_remove,
    };
    spa_zero(m_registryListener);
    auto ret = pw_registry_add_listener(m_registry.get(), &m_registryListener, &registryEvents, this);
    Q_ASSERT(ret >= 0);
}

} // namespace PipeWireQt
