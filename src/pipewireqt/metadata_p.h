// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

#pragma once

#include "pipewireobject_p.h"

#include <pipewire/extensions/metadata.h>

#include "metadata.h"

namespace PipeWireQt
{

class MetadataPrivate : public PipeWireObjectPrivate
{
public:
    using PipeWireObjectPrivate::PipeWireObjectPrivate;
    void init() override;

    int property(uint32_t subject, const char *key, const char *type, const char *value)
    {
        // qDebug() <<  "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ property";
        // qDebug() << subject << key << type << value;
        m_data.insert(QLatin1String(key), QLatin1String(value));
        Q_EMIT q->metadataChanged();
        return 0;
    }

    Metadata *q = nullptr;
    spa_hook m_listener;
    pw_metadata *m_metadata = nullptr;
    QMap<QString, QString> m_data;
};

} // namespace PipeWireQt
