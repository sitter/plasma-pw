// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

#pragma once

#include "pipewireobject.h"
#include "pipewireqt_export.h"

namespace PipeWireQt
{

class PortPrivate;

class PIPEWIREQT_EXPORT Port : public PipeWireObject
{
    Q_OBJECT
public:
    explicit Port(const std::shared_ptr<PortPrivate> &dptr, QObject *parent = nullptr);
    ~Port() override;
    Q_DISABLE_COPY_MOVE(Port)

private:
    std::shared_ptr<PortPrivate> d;
};

} // namespace PipeWireQt
