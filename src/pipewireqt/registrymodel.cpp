// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
// SPDX-FileCopyrightText: 2014-2024 Harald Sitter <sitter@kde.org>

#include "registrymodel.h"

#include "context.h"
#include "context_p.h"

namespace PipeWireQt
{

class RegistryModelPrivate
{
public:
    QObject *context = nullptr;
};

RegistryModel::RegistryModel(QObject *parent)
    : QAbstractListModel(parent)
    , d(std::make_unique<RegistryModelPrivate>())
{
}

RegistryModel::~RegistryModel() = default;

QObject *RegistryModel::context() const
{
    return d->context;
}

void RegistryModel::setContext(QObject *context)
{
    beginResetModel();
    if (d->context) {
        d->context->disconnect(this);
    }
    d->context = context;
#warning this is utter crap need to rearchitecture the way context core registry work
    auto c = qobject_cast<Context *>(d->context);
    connect(&c->d->m_core.d->m_registry, &RegistryPrivate::objectsChanged, this, [this] {
        beginResetModel();
        endResetModel();
    });
    endResetModel();
    Q_EMIT contextChanged();
}

int RegistryModel::rowCount(const QModelIndex &parent) const
{
#warning todo narrow using gsl
    auto context = qobject_cast<Context *>(d->context);
    return context->d->m_core.d->m_registry.m_objects.size();
}

QVariant RegistryModel::data(const QModelIndex &index, int role) const
{
    auto context = qobject_cast<Context *>(d->context);

    const int i = index.row();
    auto node = context->d->m_core.d->m_registry.m_objects.at(i);
    if (!node) {
        return {};
    }

    switch (static_cast<ItemRole>(role)) {
    case PipeWireObjectRole:
        return QVariant::fromValue(node.get());
    case TypeRole:
        return node->type();
    case PropertiesRole:
        return node->properties();
    }

    return {};
}

QHash<int, QByteArray> RegistryModel::roleNames() const
{
    auto names = QAbstractItemModel::roleNames();
    names.insert(PipeWireObjectRole, "pipeWireObject");
    names.insert(TypeRole, "type");
    names.insert(PropertiesRole, "properties");
    return names;
}

} // namespace PipeWireQt
